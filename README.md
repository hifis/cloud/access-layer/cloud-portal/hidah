# HIDaH - Helmholtz ID at Home

![screenshot of HIDaH 'login' form](docs/screenshot.png)

Hello there, this is HIDaH, a lightweight OpenID Connect provider emulating [Helmholtz ID](https://login.helmholtz.de) **for testing and development**. It implements a basic login flow allowing you to set the user profile attributes forwarded to your application. This way you can test how your application reacts to different attributes (e.g. group memberships) set by Helmholtz ID. Originally, it has been developed for testing [helmholtz.cloud](https://helmholtz.cloud) and by now it is in use at multiple Helmholtz Centers for testing services.

HIDaH is available as a simple container you can run alongside your development setup and provides an alternative to testing against a live environment or having to run a full OIDC/IdP setup on your machine.

## Release Management

HIDaH is a tool for development purposes. Therefore, we're following a rolling-release approach, so you can work directly from the latest main branch released as the `latest` container image. This may break your configuration from time to time, but on the other hand you'll also work with the latest updates and fixes. If you want to be notified on changes, consider using GitLab's notification feature.

## Contributing and Getting Help

If you're missing a feature or want to contribute, simply open an issue or merge request.

## Is it good?

It works reasonably well for confidential client flows, there may be a few bumps when using it with SPAs. There are some subtle differences to Helmholtz ID (e.g. requiring PKCE), which should not have much impact in practice. Sometimes your config breaks but this should be easy to fix. If you have any questions, just open an issue, features can be added as needed.

## Configuration

### Environment Variables

You must configure these environment variables so HIDaH starts up.

|Environment Variable|Description|
|--|--|
|CLIENT_ID|The OIDC client ID you want to use.|
|CLIENT_SECRET|The OIDC client secret of your client.|
|PUBLIC_CLIENT|Set to string value "true" to make your client a [public client](https://oauth.net/2/client-types/) (e.g. for SPAs).|
|OIDC_SCOPES|The scopes your client is going to request as a list of space-separated strings (e.g. "openid profile"). This does not change the `userinfo` response at this time.|
|REDIRECT_URI|Your client's redirect URI.|
|BIND_HOST|The host/port HIDaH is going to bind to (e.g. `0.0.0.0:8000`)|
|EXTERNAL_ADDRESS|The HTTP URL HIDaH will be reachable at (e.g. `https://localhost:8001/oauth2`). Useful when running HIDaH behind a reverse proxy and OIDC discovery. Roughly corresponds to issuer. No trailing slash.|
|DB_PATH|The directory HIDaH is going to persist identities to (e.g. `/hidah/identities`). Set this to an empty dir somewhere on the filesystem and ensure `UID:GID` `65534:65534` has access.|
|PRELOAD_PROFILES_PATH|Path to a JSON file containing an array of profiles to preload on startup. Format: `[ {...}, {...}, ...]`. Set to `""` to disable. For current format either use the HIDaH login form's generated profiles as a template or check the `Profile` struct in the code.|
|PRELOADED_PROFILES_OVERRIDE_STORAGE|Set to `"true"` to overwrite profiles with the data from the JSON file on startup when there's an ID collision.|

### Container Image

HIDaH is available as a container image at `registry.hzdr.de/hifis/cloud/access-layer/cloud-portal/hidah:latest`. This is a development tool, so it should be safe to use the `latest` tag.

Known identities are stored in `DB_PATH`. If you want to persist them across restarts, add a container volume. If the data format updates in the future, you may want to clear the directory to avoid parsing errors.

Consider using something like [Caddy](https://github.com/caddyserver/caddy) and set the `EXTERNAL_ADDRESS` accordingly to get TLS in local development.

#### Compose Example

You can run HIDaH as part of your local compose setup. Remember to differentiate between 'externally' reachable URLs/hostnames and 'internally' reachable ones. In many cases HIDaH must be reachable by both your local browser (so you can test the OIDC flow in your browser) and your backend application (e.g. for discovery and fetching tokens) **on the same address**. When using containers by default each container will run in it's own network namespace. Basically, each container gets its own `localhost` which in turn are completely different from your host machine's `localhost` in turn. This can prove impractical. However, containers can also share a network namespace (and `localhost`). Combine this with a simple reverse proxy like Caddy and you have the same address both inside and outside the container and you even get TLS in development!

The following example assumes your app is listening on port 3001 inside the `application` container:

![HIDaH networking example schema](docs/hidah-schema.png)


```yaml
version: '3'
name: infra
services:
  application:
    # Your application's container
    ...
    ports:
      # host -> container
      - "127.0.0.1:3000:3000"
      - "127.0.0.1:8000:8000"

  hidah:
    image: registry.hzdr.de/hifis/cloud/access-layer/cloud-portal/hidah:latest
    pull_policy: always
    restart: always
    network_mode: service:application
    environment:
      CLIENT_ID: my-client
      CLIENT_SECRET: verysecretclientsecret
      PUBLIC_CLIENT: "false"
      OIDC_SCOPES: "openid profile email eduperson_entitlement eduperson_assurance eduperson_scoped_affiliation"
      # Redirect to your app's TLS port
      REDIRECT_URI: "https://localhost:3000/callback/"
      # Listen on 8001
      BIND_HOST: 0.0.0.0:8001
      # HIDaH is 'externally' reachable on its TLS port
      EXTERNAL_ADDRESS: "https://localhost:8000/oauth2"
      DB_PATH: /hidah/identities
      PRELOAD_PROFILES_PATH: ""
      PRELOADED_PROFILES_OVERRIDE_STORAGE: "false"
    volumes:
      - /hidah/identities

  caddy:
    image: registry.hub.docker.com/library/caddy:alpine
    network_mode: service:application
    volumes:
      # Listen with TLS on 3000 and 8000 and forward to 3001 and 8001 respectively
      - ./Caddyfile:/etc/caddy/Caddyfile:z
```

Caddyfile

```Caddyfile
localhost:3000 {
	reverse_proxy localhost:3001
}

localhost:8000 {
	reverse_proxy localhost:8001
}
```

### OIDC

PKCE is mandatory for both confidential and public clients. Currently, only the `authorization_code` flow is supported.

#### Client Scopes

These are the scopes available to your client and affect the userinfo response ([attribute documentation](https://hifis.net/doc/helmholtz-aai/attributes/)).
This can be extended in the future to support additional claims on the userinfo endpoint.

- `openid`
- `email`
- `profile`
- `eduperson_assurance`
- `eduperson_entitlement`
- `eduperson_scoped_affiliation`

#### OIDC URLs

Use these URLs to configure your OIDC client application.

- OIDC discovery: `/oauth2/.well-known/openid-configuration`
- Authorization Form: `/oauth2/auth`
- Token Endpoint: `/oauth2/token`
- JWKs Endpoint: `/oauth2/jwk`
- Userinfo Endpoint: `/oauth2/userinfo`