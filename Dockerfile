FROM registry.hub.docker.com/library/golang:1.24.1 as build

COPY . /build
RUN apt-get update; apt-get dist-upgrade -y; cd /build &&\
    go get . &&\
    go install github.com/google/go-licenses@latest &&\
    go-licenses check "codebase.helmholtz.cloud/hifis/cloud/access-layer/cloud-portal/hidah" --allowed_licenses=ISC,MIT,MPL-2.0,BSD-3-Clause,BSD-2-Clause,Apache-2.0 &&\
    go-licenses save "codebase.helmholtz.cloud/hifis/cloud/access-layer/cloud-portal/hidah" --save_path="/licenses" &&\
    CGO_ENABLED=0 go build -o hidah . &&\
    echo "nobody:x:65534:65534:nobody:/_break:/sbin/nologin" > /nobody

FROM scratch

COPY --from=build /nobody /etc/passwd
COPY --from=build --chown=65534:65534 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /build/hidah /bin/hidah
COPY --from=build /licenses /licenses

USER 65534

CMD ["/bin/hidah"]
