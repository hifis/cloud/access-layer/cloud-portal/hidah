package main

import (
	"cmp"
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"slices"
	"strings"
	"time"

	gofakeit "github.com/brianvoe/gofakeit/v7"
	badger "github.com/dgraph-io/badger/v4"
	"github.com/go-jose/go-jose/v3"
	"github.com/google/uuid"
	"github.com/gorilla/sessions"
	"github.com/ory/fosite"
	"github.com/ory/fosite/compose"
	"github.com/ory/fosite/handler/openid"
	"github.com/ory/fosite/storage"
	"github.com/ory/fosite/token/jwt"
	"golang.org/x/crypto/bcrypt"
)

// HIDaH config
type Config struct {
	ClientID                         string
	ClientSecret                     string
	PublicClient                     bool
	RedirectURI                      string
	BindHost                         string
	ExternalAddress                  string
	DBPath                           string
	OIDCScopes                       string
	PreloadProfilesPath              string
	PreloadedProfilesOverrideStorage bool
}

// User profiles
type Profile struct {
	AnonymousIdentifier        string   `json:"sub"`
	Name                       string   `json:"name"`
	GivenName                  string   `json:"given_name"`
	FamilyName                 string   `json:"family_name"`
	PreferredUsername          string   `json:"preferred_username"`
	Email                      string   `json:"email"`
	EmailVerified              string   `json:"email_verified"`
	EduPersonEntitlement       []string `json:"eduperson_entitlement"`
	EduPersonAssurance         []string `json:"eduperson_assurance"`
	EduPersonScopedAffiliation []string `json:"eduperson_scoped_affiliation"`
}

// OIDC Discovery JSON
type OpenIDConfiguration struct {
	Issuer                           string   `json:"issuer"`
	AuthorizationEndpoint            string   `json:"authorization_endpoint"`
	TokenEndpoint                    string   `json:"token_endpoint"`
	UserinfoEndpoint                 string   `json:"userinfo_endpoint"`
	JWKSURI                          string   `json:"jwks_uri"`
	ScopesSupported                  []string `json:"scopes_supported"`
	GrantTypesSupported              []string `json:"grant_types_supported"`
	ResponseTypesSupported           []string `json:"response_types_supported"`
	SubjectTypesSupported            []string `json:"subject_types_supported"`
	IDTokenSigningAlgValuesSupported []string `json:"id_token_signing_alg_values_supported"`
	CodeChallengeMethodsSupported    []string `json:"code_challenge_methods_supported"`
}

// https://hifis.net/doc/helmholtz-aai/attributes/
var scopeAttributes = map[string][]string{
	"openid": {
		"sub",
	},
	"email": {
		"email",
		"email_verified",
	},
	"profile": {
		"name",
		"eduperson_entitlement",
		"given_name",
		"family_name",
		"preferred_username",
	},
	"eduperson_entitlement": {
		"eduperson_entitlement",
	},
	"eduperson_assurance": {
		"eduperson_assurance",
	},
	"eduperson_scoped_affiliation": {
		"eduperson_scoped_affiliation",
	},
}

// Persistence
// Uses BadgerDB as embedded key-value store.
// Profiles are stored as marshalled JSON representation and indexed by ID/AnonymousIdentifier/sub.
type ProfileStore struct {
	BadgerDB *badger.DB
}

func NewProfileStore(badgerDB *badger.DB) ProfileStore {
	// Initialize a new ProfileStore.
	return ProfileStore{
		BadgerDB: badgerDB,
	}
}

func (store *ProfileStore) GetProfile(id string) (*Profile, error) {
	// Load a profile from the store by ID. Returns nil if key was not found.

	var profile Profile

	// Start a new reader transaction
	err := store.BadgerDB.View(func(transaction *badger.Txn) error {

		// Load JSON from KV store and unmarshal
		item, err := transaction.Get([]byte(id))
		if err != nil {
			if err == badger.ErrKeyNotFound {
				return nil
			}

			return err
		}

		return item.Value(func(jsonProfile []byte) error {
			return json.Unmarshal(jsonProfile, &profile)
		})
	})

	if err != nil {
		return nil, err
	}

	return &profile, nil
}

func (store *ProfileStore) GetAllProfiles() (map[string]Profile, error) {
	// Get all profiles stored in the store. Map is indexed by ID.

	profiles := map[string]Profile{}

	// Start a new reader transaction
	err := store.BadgerDB.View(func(transaction *badger.Txn) error {

		// Iterate over all items in the KV store
		iterator := transaction.NewIterator(badger.DefaultIteratorOptions)
		defer iterator.Close()

		for iterator.Rewind(); iterator.Valid(); iterator.Next() {

			// Load item from store and unmarshal into profile struct
			err := iterator.Item().Value(func(jsonProfile []byte) error {
				profile := Profile{}
				err := json.Unmarshal(jsonProfile, &profile)
				if err != nil {
					return err
				}

				profiles[profile.AnonymousIdentifier] = profile

				return nil
			})

			if err != nil {
				// There was an error retrieving/unmarshaling the profile
				return err
			}
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return profiles, nil
}

func (store *ProfileStore) Store(profile Profile) error {
	// Store a profile in the store.

	// Start new write transaction
	return store.BadgerDB.Update(func(transaction *badger.Txn) error {

		// Marshal and store profile
		jsonProfile, err := json.Marshal(profile)
		if err != nil {
			return err
		}

		return transaction.Set([]byte(profile.AnonymousIdentifier), jsonProfile)
	})
}

// Global profile store instance
var profileStore ProfileStore

// Global sessions store instance (this is separate from fosite's sessions and just for checking whether a user is returning)
const WEB_SESSION_COOKIE_NAME = "__Host-hidah"

var webSessionStore *sessions.CookieStore

// Fosite OIDC provider
var provider fosite.OAuth2Provider

// Public JWK
var jwkJSON []byte

// Global config
var config Config

func tryLoadFromEnv(environmentVariableName string) string {
	// Try to load a environment variable from the environment. If it does not exist, crash.

	value, ok := os.LookupEnv(environmentVariableName)
	if !ok {
		panic(fmt.Sprintf("Failed to load environment variable %v. Please set the variable. This can happen when you're missing some recently-added configuration. Check the latest docs for current information on how to configure HIDaH: https://codebase.helmholtz.cloud/hifis/cloud/access-layer/cloud-portal/hidah", environmentVariableName))
	}

	return value
}

func loadConfig() Config {
	// Load the app's config struct from the environment. If any variables are unset, crash.

	publicClientString := tryLoadFromEnv("PUBLIC_CLIENT")
	preloadedProfilesOverrideStorageString := tryLoadFromEnv("PRELOADED_PROFILES_OVERRIDE_STORAGE")

	return Config{
		ClientID:                         tryLoadFromEnv("CLIENT_ID"),
		ClientSecret:                     tryLoadFromEnv("CLIENT_SECRET"),
		PublicClient:                     publicClientString == "true",
		RedirectURI:                      tryLoadFromEnv("REDIRECT_URI"),
		BindHost:                         tryLoadFromEnv("BIND_HOST"),
		ExternalAddress:                  tryLoadFromEnv("EXTERNAL_ADDRESS"),
		DBPath:                           tryLoadFromEnv("DB_PATH"),
		OIDCScopes:                       tryLoadFromEnv("OIDC_SCOPES"),
		PreloadProfilesPath:              tryLoadFromEnv("PRELOAD_PROFILES_PATH"),
		PreloadedProfilesOverrideStorage: preloadedProfilesOverrideStorageString == "true",
	}
}

func main() {
	// Load configuration
	config = loadConfig()

	// Initialize the BadgerDB storage
	// It is initialized externally so that closing the DB can be deferred in main()
	badgerDB, err := badger.Open(badger.DefaultOptions(config.DBPath))
	if err != nil {
		panic(fmt.Errorf("could not open profile store: %w", err))
	}
	defer badgerDB.Close()

	profileStore = NewProfileStore(badgerDB)

	// Preload/override identities from file
	preloadIdentities()

	// Initialize session store
	// The encryption key changes every start and sessions become invalid
	authenticationKey := make([]byte, 64)
	_, err = rand.Read(authenticationKey)
	if err != nil {
		panic(fmt.Errorf("could not generate authentication key: %w", err))
	}

	encryptionKey := make([]byte, 32)
	_, err = rand.Read(encryptionKey)
	if err != nil {
		panic(fmt.Errorf("could not generate encryption key: %w", err))
	}

	webSessionStore = sessions.NewCookieStore(authenticationKey, encryptionKey)
	webSessionStore.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   60 * 60 * 24,
		Secure:   true,
		HttpOnly: true,
	}

	// The OIDC library needs the client secret to be bcrypted, so bcrypt it
	bcryptedSecret, err := bcrypt.GenerateFromPassword([]byte(config.ClientSecret), bcrypt.DefaultCost)
	if err != nil {
		panic(fmt.Errorf("could not encrypt client secret: %w", err))
	}

	// Initialize the OIDC state storage in memory
	store := &storage.MemoryStore{
		Clients: map[string]fosite.Client{

			// The default client made available by HIDaH
			// Scopes could be made customizable at some point
			config.ClientID: &fosite.DefaultResponseModeClient{
				DefaultClient: &fosite.DefaultClient{ID: config.ClientID,
					Secret:         bcryptedSecret,
					RotatedSecrets: [][]byte{},
					RedirectURIs:   []string{config.RedirectURI},
					ResponseTypes: []string{
						"code",
					},
					GrantTypes: []string{
						"authorization_code",
					},
					Scopes: strings.Split(config.OIDCScopes, " "),
					Public: config.PublicClient},
				ResponseModes: []fosite.ResponseModeType{
					"query",
				},
			},
		},
		AuthorizeCodes:         map[string]storage.StoreAuthorizeCode{},
		IDSessions:             make(map[string]fosite.Requester),
		AccessTokens:           map[string]fosite.Requester{},
		RefreshTokens:          map[string]storage.StoreRefreshToken{},
		PKCES:                  map[string]fosite.Requester{},
		Users:                  map[string]storage.MemoryUserRelation{},
		BlacklistedJTIs:        map[string]time.Time{},
		AccessTokenRequestIDs:  map[string]string{},
		RefreshTokenRequestIDs: map[string]string{},
		IssuerPublicKeys:       map[string]storage.IssuerPublicKeys{},
		PARSessions:            map[string]fosite.AuthorizeRequester{},
	}

	// Generate 32 byte secret for signing
	globalSecret := make([]byte, 32)
	_, err = rand.Read(globalSecret)
	if err != nil {
		panic(fmt.Errorf("could not initialize global secret: %w", err))
	}

	// Generate a private key for signing
	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		panic(fmt.Errorf("could not initialize signing key: %w", err))
	}

	jwkJSON, err = jose.JSONWebKey{Key: &privateKey.PublicKey}.MarshalJSON()
	if err != nil {
		panic(fmt.Errorf("could not initialize jwk JSON: %w", err))
	}

	// Pre-generate response for JWKs endpoint
	jwkJSON = append(append([]byte(`{"keys":[`), jwkJSON...), []byte("]}")...)

	// Configure provider
	providerConfig := &fosite.Config{
		ScopeStrategy:                  fosite.ExactScopeStrategy,
		EnforcePKCE:                    true,
		EnforcePKCEForPublicClients:    true,
		EnablePKCEPlainChallengeMethod: false,
		GlobalSecret:                   globalSecret,
		SendDebugMessagesToClients:     true,
		OmitRedirectScopeParam:         false,
	}
	provider = compose.ComposeAllEnabled(providerConfig, store, privateKey)

	// Set up HTTP server and routes
	mux := http.NewServeMux()

	// Login form
	mux.HandleFunc("GET /oauth2/auth", loginFormHandler)

	// Login form submit handler
	mux.HandleFunc("POST /oauth2/auth", authorizeHandler)

	// Token endpoint
	mux.HandleFunc("POST /oauth2/token", tokenHandler)

	// JWKs endpoint
	mux.HandleFunc("GET /oauth2/jwk", jwkHandler)

	// Userinfo endpoint
	mux.HandleFunc("GET /oauth2/userinfo", userinfoHandler)

	// OIDC discovery endpoint
	mux.HandleFunc("GET /oauth2/.well-known/openid-configuration", oidcDiscoveryHandler)

	log.Printf("🏡 HIDaH listening at %s", config.BindHost)
	http.ListenAndServe(config.BindHost, mux)
}

func preloadIdentities() {
	//Preload idetities from file

	if config.PreloadProfilesPath == "" {
		return
	}

	log.Println("Preloading profiles from", config.PreloadProfilesPath)

	var profiles []Profile

	fileContents, err := os.ReadFile(config.PreloadProfilesPath)
	if err != nil {
		panic(fmt.Errorf("could not read profile JSON file: %w", err))
	}

	err = json.Unmarshal(fileContents, &profiles)
	if err != nil {
		panic(fmt.Errorf("could not parse profile JSON file: %w", err))
	}

	for _, profile := range profiles {
		// Test if profile exists
		existingProfile, err := profileStore.GetProfile(profile.AnonymousIdentifier)
		if err != nil {
			panic(fmt.Errorf("could not retrieve stored profile, you may have to reset the persistent store: %w", err))
		}

		// Either it does not exist or it overrides anyway
		if existingProfile == nil || config.PreloadedProfilesOverrideStorage {
			log.Println("Storing profile", profile.AnonymousIdentifier)
			profileStore.Store(profile)
		}
	}

	log.Println("Done.")
}

func jwkHandler(response http.ResponseWriter, request *http.Request) {
	// JWKs endpoint to enable clients validate signatures

	response.Header().Set("content-type", "application/json")
	_, err := response.Write(jwkJSON)

	if err != nil {
		panic(fmt.Errorf("could not send jwk JSON: %w", err))
	}
}

func loginFormHandler(response http.ResponseWriter, request *http.Request) {
	// Render the 'login' form

	// A profile containing its own marshalled representation so that the JSON can be rendered to the textarea
	type RenderedProfile struct {
		Profile Profile
		JSON    string
	}

	type TemplateParameters struct {
		SignedInProfile  *RenderedProfile
		GeneratedProfile RenderedProfile
		DBProfiles       []RenderedProfile
		IsNonePrompt     bool
	}

	// Check if prompt is 'none'
	isNonePromt := request.URL.Query().Get("prompt") == "none"

	// Prepare a generated identity which can be customized in the UI
	person := gofakeit.Person()
	email := fmt.Sprintf("%s.%s@localhost", strings.ToLower(person.FirstName), strings.ToLower(person.LastName))
	entitlement := fmt.Sprintf("urn:geant:localhost:group:hidah:%s#hidah", strings.Replace(strings.ToLower(gofakeit.Bird()), " ", "", -1))
	assurance := fmt.Sprintf("https://localhost/assurance/%s", strings.Replace(strings.ToLower(gofakeit.Fruit()), " ", "", -1))

	generatedProfile := Profile{
		AnonymousIdentifier:        uuid.New().String(),
		Name:                       fmt.Sprintf("%s %s", person.FirstName, person.LastName),
		GivenName:                  person.FirstName,
		FamilyName:                 person.LastName,
		PreferredUsername:          fmt.Sprintf("%s.%s", strings.ToLower(person.FirstName), strings.ToLower(person.LastName)),
		Email:                      email,
		EmailVerified:              email,
		EduPersonEntitlement:       []string{entitlement},
		EduPersonAssurance:         []string{assurance},
		EduPersonScopedAffiliation: []string{gofakeit.RandomString([]string{"affiliate@hidah", "staff@hidah"})},
	}

	// Marshal the generated profile so that the JSON can be rendered into the form
	generatedProfileJSONBytes, err := json.MarshalIndent(generatedProfile, "", "  ")
	if err != nil {
		panic(fmt.Errorf("could not marshal generated profile: %w", err))
	}

	// Check if user is signed-in and load profile if it exists
	var signedInProfile *RenderedProfile

	webSession, err := webSessionStore.Get(request, WEB_SESSION_COOKIE_NAME)
	if err != nil {
		// This happens on key rotation
		log.Println(fmt.Errorf("could not load session, deleting session: %w", err))

		webSession.Options.MaxAge = -1
		webSession.Save(request, response)
	}

	if signedInSub, exists := webSession.Values["sub"]; exists {
		// Fetch profile references by session from store
		profile, _ := profileStore.GetProfile(signedInSub.(string))

		if profile != nil {
			signedINProfileJSONBytes, err := json.Marshal(profile)
			if err != nil {
				panic(fmt.Errorf("could not marshal signed-in profile: %w", err))
			}

			signedInProfile = &RenderedProfile{
				Profile: *profile,
				JSON:    string(signedINProfileJSONBytes),
			}
		} else {
			// Profile is gone, delete session
			webSession.Options.MaxAge = -1
			webSession.Save(request, response)
		}
	}

	// Load all known profiles from the store
	allProfilesMap, err := profileStore.GetAllProfiles()
	if err != nil {
		panic(fmt.Errorf("could not load all profiles: %w", err))
	}

	// Prepare marshalled known profiles
	var allProfiles []RenderedProfile
	for profileID := range allProfilesMap {
		knownProfileJSONBytes, err := json.MarshalIndent(allProfilesMap[profileID], "", "  ")
		if err != nil {
			panic(fmt.Errorf("could not marshal profile: %w", err))
		}

		allProfiles = append(allProfiles, RenderedProfile{
			Profile: allProfilesMap[profileID],
			JSON:    string(knownProfileJSONBytes),
		})
	}

	// Sort by name
	slices.SortFunc(allProfiles, func(profileA, profileB RenderedProfile) int {
		return cmp.Compare(profileA.Profile.Name, profileB.Profile.Name)
	})

	// Put it all together in the template parameters and render template
	templateParameters := TemplateParameters{
		SignedInProfile: signedInProfile,
		GeneratedProfile: RenderedProfile{
			Profile: generatedProfile,
			JSON:    string(generatedProfileJSONBytes),
		},
		DBProfiles:   allProfiles,
		IsNonePrompt: isNonePromt,
	}

	loginTemplate, err := template.New("LoginForm").Parse(`
	<!doctype html>
	<html>
	<head>
	<title>🏡 HIDaH Identity</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<style>
	body {
		font-family: sans-serif;
		text-align: center;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		background-color: #002864;
		color: #002864;
		min-height: 100vh;
		margin: 0;
		font-size: 1.0rem;
	}
	h2 {
		margin-top: 4rem;
	}
	input[type=submit] {
		background-color: #002864;
		border-radius: 5px;
		color: #fff;
		padding: .5rem 1rem;
		margin-top: .2rem;
		margin-left: 1rem;
		font-size: 1.1rem;
		font-weight: 500;
		border-style: none;
		cursor: pointer;
	}
	textarea {
		font-family: monospace;
		border-color: #777;
		border-radius: 5px;
		border-width: 1px;
		padding: 1rem;
		margin-top: 0;
		margin-bottom: 0;
		font-size: .9rem;
		font-weight: 500;
		line-height: 1.5;
		background-color: #113;
		color: #fff;
	}
	summary {
		font-family: monospace;
		cursor: pointer;
	}
	.login-form {
		background-color: #fff;
		border-radius: 5px;
		padding: 3rem 3rem 2rem;
		margin-top: 1rem;
		margin-bottom: 1rem;
	}
	.home-icon {
		margin: 0;
		font-size: 4rem;
	}
	.form-stored-identity {
		display: flex;
		flex-direction: row;
		background-color: #eeeeef;
		justify-content: space-between;
		border-radius: 5px;
		padding: 1rem;
		text-align: left;
	}
	.form-current-identity {
		display: flex;
		flex-direction: row;
		background-color: #dbf2e0;
		border: 1px solid;
		border-color: #8ae79d;
		justify-content: space-between;
		border-radius: 5px;
		padding: 1rem;
		text-align: left;
	}
	.profile {
		margin-top: 1rem;
	}
	</style>
	</head>
	<body>
	<div class="login-form">
	<h1 class="home-icon">🏡</h1>
	<h1>HIDaH Identity</h1>
{{if .SignedInProfile}}
{{if .IsNonePrompt}}
	<p>
		You're signed into HIDaH and the relying party requested the flow with <tt>prompt = none</tt>.<br />
		Normally, you'd be forwarded automatically. Click 'Continue' to continue.
	</p>
{{ else }}
	<p>You're signed in as</p>
{{ end }}
	<form method="post" class="form-current-identity">
		<div>
			<span><strong>{{ .SignedInProfile.Profile.Name }}</strong></span>
			<details>
				<summary>{{ .SignedInProfile.Profile.AnonymousIdentifier }}</summary>
				<textarea cols="40" rows="15" name="claims">
{{ .SignedInProfile.JSON }}
				</textarea>
			</details>
		</div>
		<div>
			<input type="submit" value="Continue">
		</div>
	</form>
{{ else }}
{{if .IsNonePrompt}}
	<p>
		You're not signed into HIDaH, but the relying party requested <tt>prompt = none</tt>.<br />
		There is no way to forward you automatically with the profile expected by the relying party.<br />
		Pick a profile below but remember switching accounts here is probably not expected by the relying party.
	</p>
{{ end }}
{{ end }}
{{if or (eq .IsNonePrompt false) (eq .SignedInProfile nil)}}
{{if .DBProfiles}}
	<h2>Known Identities</h2>
{{ range $index, $renderedProfile := .DBProfiles }}
<div class="profile">
<form method="post" class="form-stored-identity">
	<div>
		<span><strong>{{ $renderedProfile.Profile.Name }}</strong></span>
		<details>
			<summary>{{ $renderedProfile.Profile.AnonymousIdentifier }}</summary>
			<textarea cols="40" rows="15" name="claims">
{{ $renderedProfile.JSON }}
			</textarea>
		</details>
	</div>
	<div>
		<input type="submit" value="Sign In">
	</div>
</form>
</div>
{{ end }}
{{ end }}
	<h2>Create New Identity</h2>
	<form method="post">
		<textarea cols="50" rows="15" name="claims" data-testid="generated-identity-claims">
{{ .GeneratedProfile.JSON }}
		</textarea><br/ ><br />
		<input type="submit" value="Sign In" data-testid="generated-identity-sign-in-button">
	</form>
{{ end }}
	</div>
	</body>
	</html>
	`)
	if err != nil {
		panic(fmt.Errorf("could not parse template: %w", err))
	}

	// Write template to HTTP response
	response.Header().Set("content-type",
		"text/html;charset=UTF-8")
	err = loginTemplate.Execute(response, templateParameters)

	if err != nil {
		panic(fmt.Errorf("could not render template: %w", err))
	}
}

func authorizeHandler(response http.ResponseWriter, request *http.Request) {
	// Form submit handler of login form

	// Check if session is valid
	webSession, err := webSessionStore.Get(request, WEB_SESSION_COOKIE_NAME)
	if err != nil {
		log.Println(fmt.Errorf("could not load session for sign-in, deleting session: %w", err))

		webSession.Options.MaxAge = -1
		webSession.Save(request, response)

		http.Error(response, "You could not be signed-in due to your HIDaH session being broken. This can happen when HIDaH was restarted during your login attempt. Please try again.", 500)
		return
	}

	// Load request
	authorizeRequester, err := provider.NewAuthorizeRequest(request.Context(), request)
	if err != nil {
		log.Println(fmt.Errorf("authorization request error: %w", err))
		provider.WriteAuthorizeError(request.Context(), response, authorizeRequester, err)
		return
	}

	// Grant all scopes
	var scopes []string

	for _, scope := range authorizeRequester.GetRequestedScopes() {
		authorizeRequester.GrantScope(scope)
		scopes = append(scopes, scope)
	}

	// Write session data
	// Parse form input and persist it in store
	claimsJSON := request.FormValue("claims")

	profile := Profile{}
	err = json.Unmarshal([]byte(claimsJSON), &profile)
	if err != nil {
		panic(fmt.Errorf("could not parse input JSON: %w", err))
	}

	err = profileStore.Store(profile)
	if err != nil {
		panic(fmt.Errorf("could not store profile: %w", err))
	}

	now := time.Now()

	oidcSession := &openid.DefaultSession{
		Subject: profile.AnonymousIdentifier,
		Claims: &jwt.IDTokenClaims{
			Issuer:      config.ExternalAddress,
			Subject:     profile.AnonymousIdentifier,
			Audience:    []string{},
			ExpiresAt:   time.Now().Add(time.Hour * 24),
			IssuedAt:    now,
			RequestedAt: now,
			AuthTime:    now,
			Extra: map[string]interface{}{
				"scopes": strings.Join(scopes, " "),
			},
		},
	}

	// Write web session to cookie
	webSession.Values["sub"] = profile.AnonymousIdentifier
	webSession.Save(request, response)

	// Write response
	authorizeResponse, err := provider.NewAuthorizeResponse(request.Context(), authorizeRequester, oidcSession)
	if err != nil {
		log.Println(fmt.Errorf("authorization response error: %w", err))
		provider.WriteAuthorizeError(request.Context(), response, authorizeRequester, err)
		return
	}
	provider.WriteAuthorizeResponse(request.Context(), response, authorizeRequester, authorizeResponse)
}

func tokenHandler(response http.ResponseWriter, request *http.Request) {
	// OIDC token endpoint

	oidcSession := openid.NewDefaultSession()

	// Load request
	accessRequest, err := provider.NewAccessRequest(request.Context(), request, oidcSession)
	if err != nil {
		log.Println(fmt.Errorf("error loading access request: %w", err))
		provider.WriteAccessError(request.Context(), response, accessRequest, err)
		return
	}

	// Grant all scopes on request
	for _, scope := range accessRequest.GetRequestedScopes() {
		accessRequest.GrantScope(scope)
	}

	// Write response
	accessResponse, err := provider.NewAccessResponse(request.Context(), accessRequest)
	if err != nil {
		log.Println(fmt.Errorf("error sending access response: %w", err))
		provider.WriteAccessError(request.Context(), response, accessRequest, err)
		return
	}
	provider.WriteAccessResponse(request.Context(), response, accessRequest, accessResponse)
}

func userinfoHandler(response http.ResponseWriter, request *http.Request) {
	// OIDC userinfo endpoint

	// Introspect token to determine if token is authorized and to which user this token belongs to
	_, accessRequest, err := provider.IntrospectToken(
		request.Context(),
		fosite.AccessTokenFromRequest(request),
		fosite.AccessToken, new(fosite.DefaultSession),

		// Validate token has at least these scopes
		strings.Split(config.OIDCScopes, " ")...,
	)
	if err != nil {
		panic(fmt.Errorf("could not introspect token: %w", err))
	}

	// Fetch profile from store, select the attributes by scopes and marshal it for response
	userID := accessRequest.GetSession().GetSubject()
	profile, err := profileStore.GetProfile(userID)
	if err != nil {
		panic(fmt.Errorf("could not get user from store: %w", err))
	}

	if profile == nil {
		panic(fmt.Errorf("user does not exist in store"))
	}

	jsonBytes, err := json.Marshal(profile)
	if err != nil {
		panic(fmt.Errorf("could not marshal profile: %w", err))
	}

	// Unmarshal into map to select fields
	var profileMap map[string]interface{}

	err = json.Unmarshal(jsonBytes, &profileMap)
	if err != nil {
		panic(fmt.Errorf("could not unmarshal profile map: %w", err))
	}

	userinfoMap := make(map[string]interface{})

	for _, scope := range accessRequest.GetGrantedScopes() {
		for _, attribute := range scopeAttributes[scope] {
			userinfoMap[attribute] = profileMap[attribute]
		}
	}

	userinfoBytes, err := json.Marshal(userinfoMap)
	if err != nil {
		panic(fmt.Errorf("could not marshal userinfo: %w", err))
	}

	response.Header().Set("content-type", "application/json")
	_, err = response.Write(userinfoBytes)

	if err != nil {
		panic(fmt.Errorf("could not send userinfo JSON: %w", err))
	}
}

func oidcDiscoveryHandler(response http.ResponseWriter, request *http.Request) {
	// OIDC discovery endpoint

	supportedScopes := make([]string, 0, len(scopeAttributes))
	for scope := range scopeAttributes {
		supportedScopes = append(supportedScopes, scope)
	}

	discoveryDocument := OpenIDConfiguration{
		Issuer:                           config.ExternalAddress,
		AuthorizationEndpoint:            fmt.Sprintf("%s/auth", config.ExternalAddress),
		TokenEndpoint:                    fmt.Sprintf("%s/token", config.ExternalAddress),
		UserinfoEndpoint:                 fmt.Sprintf("%s/userinfo", config.ExternalAddress),
		JWKSURI:                          fmt.Sprintf("%s/jwk", config.ExternalAddress),
		ScopesSupported:                  supportedScopes,
		ResponseTypesSupported:           []string{"code"},
		GrantTypesSupported:              []string{"authorization_code"},
		SubjectTypesSupported:            []string{"public"},
		IDTokenSigningAlgValuesSupported: []string{"RS256"},
		CodeChallengeMethodsSupported:    []string{"S256"},
	}

	discoveryBytes, err := json.Marshal(discoveryDocument)
	if err != nil {
		panic(fmt.Errorf("could not marshal OIDC discovery JSON: %w", err))
	}

	response.Header().Set("content-type", "application/json")
	_, err = response.Write(discoveryBytes)

	if err != nil {
		panic(fmt.Errorf("could not send OIDC discovery JSON: %w", err))
	}
}
